package main

import (
	"fmt"

	"gitlab.com/SiberianPanda/selfcard_service/src/pkg/server"
)

func main() {
	go server.Up(":8080")
	fmt.Scanln()
}
